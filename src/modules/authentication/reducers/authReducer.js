import * as actions from "../actions/authActions";

const defaultState = {
  currentStatus: 'filling',
  errorMessage: null,
};

const authReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.GET_TOKEN_REQUEST:
      return { ...state, currentStatus: "loading" };
    case actions.GET_TOKEN_SUCCESS:
      return { ...state, currentStatus: "success" };
    case actions.GET_TOKEN_FAILURE:
      return {
        ...state,
        currentStatus: "failed",
        errorMessage: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export default authReducer;