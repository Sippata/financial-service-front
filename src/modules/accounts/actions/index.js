export const GET_ACCOUNTS_REQUEST = 'GET_ACCOUNTS_REQUEST';
export const GET_ACCOUNTS_SUCCESS = 'GET_ACCOUNTS_SUCCESS';
export const GET_ACCOUNTS_FAILURE = 'GET_ACCOUNTS_FAILURE';

export const getAccountsRequest = () => ({
  type: GET_ACCOUNTS_REQUEST,
});

export const getAccountsSuccess = (payload) => ({
  type: GET_ACCOUNTS_SUCCESS,
  payload,
});

export const getAccountsFailure = (payload) => ({
  type: GET_ACCOUNTS_FAILURE,
  payload,
});

export const OPEN_ACCOUNT_REQUEST = 'OPEN_ACCOUNT_REQUEST';
export const OPEN_ACCOUNT_SUCCESS = 'OPEN_ACCOUNT_SUCCESS';
export const OPEN_ACCOUNT_FAILURE = 'OPEN_ACCOUNT_FAILURE';

export const openAccountRequest = () => ({
  type: OPEN_ACCOUNT_REQUEST,
});

export const openAccountSuccess = (payload) => ({
  type: OPEN_ACCOUNT_SUCCESS,
  payload,
});

export const openAccountFailure = (payload) => ({
  type: OPEN_ACCOUNT_FAILURE,
  payload,
});

export const CHANGE_CURRENT_ACCOUNT = 'CHANGE_CURRENT_ACCOUNT';

export const changeCurrentAccount = (payload) => ({
  type: CHANGE_CURRENT_ACCOUNT,
  payload
});

export const CLOSE_ACCOUNT = 'CLOSE_ACCOUNT';

export const closeAccount = (payload) => ({
  type: CLOSE_ACCOUNT,
  payload
});

export const TOP_UP_REQUEST = 'TOP_UP_REQUEST';
export const TOP_UP_SUCCESS = 'TOP_UP_SUCCESS';
export const TOP_UP_FAILURE = 'TOP_UP_FAILURE';

export const topUpRequest = (payload) => ({
  type: TOP_UP_REQUEST,
  payload
});

export const topUpSuccess = (payload) => ({
  type: TOP_UP_SUCCESS,
  payload,
});

export const topUpFailure = (payload) => ({
  type: TOP_UP_FAILURE,
  payload,
});


export const TRANSFER_REQUEST = 'TRANSFER_REQUEST';
export const TRANSFER_SUCCESS = 'TRANSFER_SUCCESS';
export const TRANSFER_FAILURE = 'TRANSFER_FAILURE';

export const transferRequest = (payload) => ({
  type: TRANSFER_REQUEST,
  payload
});

export const transferSuccess = (payload) => ({
  type: TRANSFER_SUCCESS,
  payload,
});

export const transferFailure = (payload) => ({
  type: TRANSFER_FAILURE,
  payload,
});