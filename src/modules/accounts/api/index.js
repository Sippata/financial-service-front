import axios from '../../../axiosConfig';

export const fetchAccounts = () => {
  return axios.get('/accounts')
    .then(({ data }) => data);
}

export const fetchOpenAccount = () => {
  return axios.post('/accounts')
    .then(({ data }) => data);
}

export const fetchCloseAccount = (number) => {
  return axios.delete(`/accounts/${number}`);
}

export const fetchTopUp = (topUpData) => {
  return axios.patch('/accounts/top_up', topUpData)
    .then(({ data }) => data)
}

export const fetchTransfer = (transferData) => {
  return axios.patch('/accounts/transfer', transferData)
    .then(({ data }) => data);
}