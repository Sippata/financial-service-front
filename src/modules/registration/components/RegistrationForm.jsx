import React from 'react';
import { Form, Button, Input } from 'antd';

const layout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 4,
  },
};

const tailLayout = {
  wrapperCol: {
    offset: 10,
    span: 10,
  },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
  },
};

const RegistrationForm = (props) => {
  const { onSubmit } = props;
  return (
    // TODO: interpreter case 'error' from server.
    <Form
      {...layout}
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onSubmit}
      validateMessages={validateMessages}
    >
      <Form.Item
        label="E-mail"
        name="email"
        required
        rules={[
          {
            type: 'email',
            required: true,
          },
        ]}
      >
        <Input placeholder="E-mail" />
      </Form.Item>

      <Form.Item
        label="Username"
        name="username"
        rules={[
          { required: true },
          { 
            validator: (rule, value) => {
              if (value && value.length < 6) {
                return Promise.reject("Username must be at least 6 characters.")
              }
              return Promise.resolve();
            }
          }
        ]}
      >
        <Input placeholder="Username" />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        required
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input.Password placeholder="Password" />
      </Form.Item>

      <Form.Item
        name="confirmPassword"
        label="Confirm Password"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject('The two passwords that you entered do not match!');
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button
          type="primary"
          htmlType="submit"
        >
          SignUp
        </Button>
      </Form.Item>
    </Form>
  );
};

export default RegistrationForm;