import React, { useEffect } from 'react';
import { Select, Spin, Button, Popconfirm, Row, Col } from 'antd';
import AccountManager from './AccountManager';
import * as actions from '../actions/index';
import { connect } from 'react-redux';

const mapDispatchToProps = {
  getAccounts: actions.getAccountsRequest,
  openNewAccount: actions.openAccountRequest,
  changeCurrentAccount: actions.changeCurrentAccount,
};

const mapStateToProps = (state) => {
  const { byId, allIds, errorMessage, status } = state.accounts;
  return { byId, allIds, errorMessage, status };
};

const renderSelector = (props) => {
  const { byId, allIds } = props;

  if (allIds.length <= 1) {
    return null;
  }

  return (  
    <Select onChange={generateOnChange(props)} defaultValue={byId[allIds[0]].number} >
      {
        allIds.map((id) => (
          <Select.Option key={id} value={id}>
            {byId[id].number}
          </Select.Option>
        ))
      }
    </Select>
  );
};

const renderButton = ({ onConfirm }) => {
  const title = "Дейсвительно хотите открыть новый счет?"
  return (
    <Popconfirm onConfirm={onConfirm} title={title}>
      <Button>
        Открыть счет
      </Button>
    </Popconfirm>
  );
};

const generateOnConfirm = (props) => () => {
  props.openNewAccount();
};

const generateOnChange = (props) => (id) => {
  props.changeCurrentAccount({ id });
}

// Statuses: loading, redy
const Accounts = (props) => {
  const { status, getAccounts } = props;

  useEffect(() => {
    getAccounts();
  }, []);
  
  switch (status) {
    case "loading":
      return (
        <Row justify='center' align="middle">
          <Spin spinning size="large" />
        </Row>
      );
    case "redy":
      return (
        <Row >
          <Col span={16}>
            <AccountManager />
          </Col>
          {renderSelector(props)}
          {renderButton({ onConfirm: generateOnConfirm(props) })}
        </Row>
      )
    case "failure":
      return (
        <Row>
          <h1>Could not upload the data.</h1>
        </Row>
      )
    default:
      throw new Error(`Unknown status in Accounts: ${status}`);
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Accounts);