export const REGISTRATION_REQUEST = 'REGISTRATION_REQUEST';
export const REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
export const REGISTRATION_FAILURE = 'REGISTRATION_FAILURE';

export const registrationRequest = (payload) => ({
  type: REGISTRATION_REQUEST,
  payload,
});

export const registrationSuccess = (payload) => ({
  type: REGISTRATION_SUCCESS,
  payload,
});

export const registrationFailure = (payload) => ({
  type: REGISTRATION_FAILURE,
  payload,
});