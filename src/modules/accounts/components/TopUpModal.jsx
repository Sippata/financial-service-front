import React from 'react';
import { Modal, Form, Input, InputNumber } from 'antd';

const TopUpModal = (props) => {
  const { visible, onClose, onCreate, accountNumber } = props;
  const [form] = Form.useForm();
  return ( 
    <Modal
      title="TopUp"
      visible={visible}
      onCancel={onClose}
      okText="Submit"
      onOk={() => {
        form.validateFields()
          .then(values => {
            console.log(values);
            form.resetFields();
            onCreate(values);
          });
      }}
    >
      <Form
        form={form}
        name="top_up"
        initialValues={{
                     accountNumber
                    }}
      >
        <Form.Item
          label="Account Number"
          name="accountNumber"
        >
          <Input readOnly />
        </Form.Item>

        <Form.Item
          label="Amount"
          name="amount"
          rules={[
              {
                required: true,
              }
            ]}
        >
          <InputNumber style={{ width: '100%'}} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default TopUpModal;