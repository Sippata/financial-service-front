import React from 'react';
import Login from './modules/authentication/components/Login';
import PrivateRoute from './modules/authentication/containers/PrivateRoute';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import Registration from './modules/registration/components/Registartion';
import Accounts from './modules/accounts/components/Accounts';

const App = (props) => {
  return (
    <BrowserRouter>
      <div>
        <Switch>          
          <Route exact path={'/'} >
            <PrivateRoute>
              <Accounts />
            </PrivateRoute>
          </Route>

          <Route exact path={'/login'} >
            <Login />
          </Route>

          <Route exact path={'/signup'} >
            <Registration />
          </Route>

          {/* Default redirect */}
          <Redirect to={'/'} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
