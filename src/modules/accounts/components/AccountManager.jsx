import React, { useState } from 'react';
import { Card, Col, Row, Button, Popconfirm } from 'antd';
import { connect } from 'react-redux';
import * as actions from '../actions/index';
import TopUpModal from './TopUpModal';
import TransferModal from './TransferModal';

const mapDispatchToProps = {
  closeAccount: actions.closeAccount,
  topUp: actions.topUpRequest,
  transfer: actions.transferRequest,
}

const mapStateToProps = (state) => {
  return {
    currentAccountId: state.accounts.currentAccountId,
    byId: state.accounts.byId,
  }
}

const AccountManager = (props) => {

  const [modalVisible, setModalVisible] = useState(null);

  const confirmText = "Вы уверенны, что хотите закрыть счет?"
  const { currentAccountId, byId, closeAccount, topUp, transfer } = props;
  const account = byId[currentAccountId];
  return !account ? null :
    <>
      <Card title={account.number}>
        <Row>
          <Col span={8}>Баланс:</Col>
          <Col span={8}>{account.balance}</Col>
        </Row>
        <Row>
          <Col span={4}>
            <Button onClick={() => setModalVisible("topup")}>Пополнить</Button>
          </Col>
          <Col span={4}>
            <Button onClick={() => setModalVisible("transfer")}>Перевод</Button>
          </Col>
        </Row>
        <Row>
          <Popconfirm
            placement="bottomRight"
            title={confirmText}
            onConfirm={() => closeAccount(account)}
          >
            <Button>Закрыть счет</Button>
          </Popconfirm>
        </Row>
      </Card>
      <TopUpModal 
        visible={modalVisible === "topup"}
        onCreate={topUp}
        onClose={() => setModalVisible(null)} 
        accountNumber={account.number}
      />
      <TransferModal
        visible={modalVisible === "transfer"}
        onClose={() => setModalVisible(null)}
        onCreate={transfer}
        accountNumber={account.number}
      />
    </>
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountManager);