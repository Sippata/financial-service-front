import React from 'react';
import { Redirect } from "react-router-dom";

const PrivateRoute = (props) => {
  const { children } = props;
  const token = sessionStorage.getItem('token');
  return (token && token !== "undefined") ? children : <Redirect to="/login" />
};

export default PrivateRoute;