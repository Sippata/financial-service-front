import axios from '../../../axiosConfig';

export const fetchToken = (authData) => {
  return axios.post('/profile/token', authData)
    .then(({ data }) => data)
    .catch((error) => {
      if (error.response) {
        throw new Error(error.response.data.error);
      }
    });
};