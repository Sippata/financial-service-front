import * as actions from '../actions/index';
import { fetchAccounts, fetchOpenAccount, fetchCloseAccount, fetchTopUp, fetchTransfer } from '../api/index';
import { put, call, takeEvery } from 'redux-saga/effects';

function* getAccounts() {
  try {
    const data = yield call(fetchAccounts);
    const allIds = data.map(({ id }) => id);
    const byId = data.reduce((acc, account) => ({ ...acc, [account.id]: account}), {});
    yield put(actions.getAccountsSuccess({ allIds, byId }));
  } catch (error) {
    yield put(actions.getAccountsFailure(error.message));
  }
};

function* openAccount() {
  try {
    const data = yield call(fetchOpenAccount);
    yield put(actions.openAccountSuccess({ data }));
  } catch (error) {
    yield put(actions.openAccountFailure(error.message));
  }
}

function* closeAccount(action) {
  yield call(fetchCloseAccount, action.payload.number)
}

function* topUp(action) {
  try {
    const data = yield call(fetchTopUp, action.payload);
    yield put(actions.topUpSuccess({ data }));
  } catch (error) {
    yield put(actions.topUpFailure(error.message));
  }
}

function* transfer(action) {
  try {
    const data = yield call(fetchTransfer, action.payload);
    yield put(actions.transferSuccess({ data }));
  } catch (error) {
    yield put(actions.transferFailure(error.message));
  }
}

function* accountSagas() {
  yield takeEvery(actions.GET_ACCOUNTS_REQUEST, getAccounts);
  yield takeEvery(actions.OPEN_ACCOUNT_REQUEST, openAccount);
  yield takeEvery(actions.CLOSE_ACCOUNT, closeAccount);
  yield takeEvery(actions.TOP_UP_REQUEST, topUp);
  yield takeEvery(actions.TRANSFER_REQUEST, transfer);
};

export { accountSagas };