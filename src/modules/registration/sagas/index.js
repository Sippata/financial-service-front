import { fetchRegistration } from '../api/index';
import * as actions from '../actions/index';
import { takeEvery, call, put } from 'redux-saga/effects';

function* registerUser(action) {
  try {
    yield call(fetchRegistration, action.payload);
    yield put(actions.registrationSuccess());
  } catch (error) {
    yield put(actions.registrationFailure({ errorMessage: error.message }))
  }
}

function* registrationSagas() {
  yield takeEvery(actions.REGISTRATION_REQUEST, registerUser);
}

export { registrationSagas }