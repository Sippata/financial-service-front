import React from 'react';
import { registrationRequest } from '../actions/index';
import { Redirect } from 'react-router-dom';
import RegistrationForm from './RegistrationForm';
import { connect } from 'react-redux';

const mapDispatchToProps = {
  registrationRequest,
}

const mapStateToProps = (state) => {
  const props = {
    errorMessage: state.registration.errorMessage,
    status: state.registration.currentStatus,
  }
  return props;
};

const Registration = (props) => {
  const { registrationRequest, status, errorMessage } = props;
  const handleSubmit = (values) => {
    registrationRequest(values);
  };

  return (
    status === "success"
      ? <Redirect to='/login' />
      : <RegistrationForm onSubmit={handleSubmit} error={errorMessage} />
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);