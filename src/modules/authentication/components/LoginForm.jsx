import React from 'react';
import { Form, Button, Input } from 'antd';
import { Link } from 'react-router-dom';

const layout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 4,
  },
};

const tailLayout = {
  wrapperCol: {
    offset: 10,
    span: 10,
  },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
  },
};

const LoginForm = (props) => {
  const { onSubmit, status } = props;

  return (
    <Form
      {...layout}
      name="login"
      onFinish={onSubmit}
      validateMessages={validateMessages}
    >
      <Form.Item
        label="Login"
        name="email"
        rules={[
          {
            type: 'email',
            required: true,
          },
        ]}
      >
        <Input placeholder="Email" />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input.Password placeholder="Password" />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button
          type="primary"
          htmlType="submit"
          loading={status === "loading"}
        >
          Войти
        </Button>
        <Button type="link">
          <Link to="/signup">Регистрация</Link>
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginForm;