import axios from 'axios';

const instance = axios.create({ baseURL: process.env.REACT_APP_API_URL });

instance.defaults.headers.common['Access-Control-Origin'] = '*';
instance.defaults.headers.post['Content-Type'] = 'application/json';

instance.interceptors.request.use((config) => {
  const token = sessionStorage.getItem('token');
  if (token && token !== 'undefined') {
    config.headers.Authorization = 'Bearer ' + token;
  }
  return config;
});

export default instance;