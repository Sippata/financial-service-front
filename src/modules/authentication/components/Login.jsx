import React from 'react';
import LoginForm from "./LoginForm";
import * as actions from "../actions/authActions";
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom';

const mapDispatchToProps = {
  getTokenRequest: actions.getTokenRequest,
};

const mapStateToProps = (state) => {
  const props = {
    currentStatus: state.auth.currentStatus,
  };
  return props;
};

const Login = (props) => {
  const handleSubmit = (values) => {
    props.getTokenRequest(values);
  };

  const { currentStatus } = props;
  return currentStatus === "success"
    ? <Redirect to='/' />
    : <LoginForm onSubmit={handleSubmit} status={currentStatus} /> 
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);