import { combineReducers } from 'redux';
import authReducer from './modules/authentication/reducers/authReducer';
import registrationReducer from './modules/registration/reducers/index';
import accountsReducer from './modules/accounts/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  registration: registrationReducer,
  accounts: accountsReducer,
});

export default rootReducer;