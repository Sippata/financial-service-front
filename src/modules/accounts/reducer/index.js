import * as actions from '../actions/index';

const defaultState = {
  status: "loading",
  byId: {},
  allIds: [],
  errorMessage: null,
  currentAccountId: null,
};

const accountsReducer = (state = defaultState, { type, payload }) => {
  switch (type) {
    case actions.GET_ACCOUNTS_REQUEST:
      return { ...state, status: "loading" };

    case actions.GET_ACCOUNTS_SUCCESS:
      return { 
        ...state,
        byId: payload.byId,
        allIds: payload.allIds,
        status: "redy",
        currentAccountId: payload.allIds[0]
      };

    case actions.GET_ACCOUNTS_FAILURE:
      return {
        ...state,
        status: "failure",
        errorMessage: payload.error
      };

    case actions.OPEN_ACCOUNT_SUCCESS:
      const { data } = payload;
      return {
        ...state,
        byId: {...state.byId, [data.id]: data},
        allIds: [...state.allIds, data.id]
      };

    case actions.OPEN_ACCOUNT_FAILURE:
      return { ...state, errorMessage: payload.error }

    case actions.CHANGE_CURRENT_ACCOUNT:
      return { ...state, currentAccountId: payload.id }

    case actions.CLOSE_ACCOUNT: {
      const allIds = state.allIds.filter(id => payload.id !== id);
      const byId = allIds.reduce((acc, id) => ({ ...acc, [id]: state.byId[id] }), {})
      return { 
        ...state,
        allIds,
        byId,
        currentAccountId: allIds[0]
      }
    }
    
    case actions.TOP_UP_REQUEST:
    case actions.TRANSFER_REQUEST:
      return state;

    case actions.TOP_UP_SUCCESS:
    case actions.TRANSFER_SUCCESS: {
      const { data: account } = payload;
      const byId = { ...state.byId, [account.id]: account };
      return { ...state, byId };
    }

    case actions.TOP_UP_FAILURE:
    case actions.TRANSFER_FAILURE:
      return { ...state, errorMessage: payload.error }

    default:
      return state;
  }
};

export default accountsReducer;