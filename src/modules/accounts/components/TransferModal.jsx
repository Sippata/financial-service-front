import React from 'react';
import { Modal, Form, Input, InputNumber } from 'antd';

const TransferModal = (props) => {
  const { visible, onClose, onCreate, accountNumber } = props;
  const [form] = Form.useForm();
  return ( 
    <Modal
      title="Transfer"
      visible={visible}
      onCancel={onClose}
      okText="Submit"
      onOk={() => {
        form.validateFields()
          .then(values => {
            console.log(values);
            form.resetFields();
            onCreate(values);
          });
      }}
    >
      <Form
        form={form}
        name="transfer"
        initialValues={
          {
            transferFromNumber: accountNumber
          }
        }
      >
        <Form.Item
          label="From account number"
          name="transferFromNumber"
        >
          <Input readOnly />
        </Form.Item>

        <Form.Item
          label="To account number"
          name="transferToNumber"
          rules={[
            {
              required: true,
              validator: (rule, value) => {
                const regex = /^4\d{9}$/gm;
                if (regex.test(value)) {
                  return Promise.resolve();
                }
                return Promise.reject('Not match 4_XXX_XXX_XXX')
              }
            }
          ]}
        >
          <InputNumber style={{ width: '100%'}}/>
        </Form.Item>

        <Form.Item
          label="Amount"
          name="amount"
          rules={[
              {
                required: true,
              }
            ]}
        >
          <InputNumber style={{ width: '100%'}} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default TransferModal;