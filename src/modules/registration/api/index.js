import axios from '../../../axiosConfig';

export const fetchRegistration = (userData) => {
  return axios.post('/profile/registration', userData)
    .then(({ data }) => data)
    .catch((error) => {
      if (error.response) {
        throw new Error(error.response.data.error);
      }
    })
}