import * as actions from '../actions/index';

const defaultState = {
  currentStatus: "filling"
};

const registrationReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.REGISTRATION_REQUEST:
      return { currentStatus: "loading" }
    case actions.REGISTRATION_SUCCESS:
      return { currentStatus: "success"}
    case actions.REGISTRATION_FAILURE:
      return { currentStatus: "failure"}
  
    default:
      return state;
  }
};

export default registrationReducer;