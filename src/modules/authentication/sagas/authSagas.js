import { put, call, takeEvery } from "redux-saga/effects";
import { fetchToken } from '../api/authApi';
import * as actions from '../actions/authActions';

function* getToken(action) {
  try {
    const data = yield call(fetchToken, action.payload);
    //TODO: add XSS and CSRF protection
    sessionStorage.setItem('token', data.token);
    yield put(actions.getTokenSuccess({ data }));
  } catch (error) {
    yield put(actions.getTokenFailure({ errorMessage: error.message }));
  }
}

function* authSagas() {
  yield takeEvery(actions.GET_TOKEN_REQUEST, getToken);
}

export { authSagas }