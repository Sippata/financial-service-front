export const GET_TOKEN_REQUEST = 'GET_TOKEN_REQUEST';
export const GET_TOKEN_SUCCESS = 'GET_TOKEN_SUCCESS';
export const GET_TOKEN_FAILURE = 'GET_TOKEN_SUCCESS';

export const getTokenRequest = (payload) => ({
  type: GET_TOKEN_REQUEST,
  payload
});
export const getTokenSuccess = (payload) => ({
  type: GET_TOKEN_SUCCESS,
  payload
});
export const getTokenFailure = (payload) => ({
  type: GET_TOKEN_FAILURE,
  payload
});
