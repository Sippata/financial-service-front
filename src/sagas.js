import { authSagas } from './modules/authentication/sagas/authSagas';
import { registrationSagas } from './modules/registration/sagas/index';
import { all } from 'redux-saga/effects';
import { accountSagas } from './modules/accounts/sagas';

function* rootSaga() {
  yield all([
    authSagas(),
    registrationSagas(),
    accountSagas(),
  ]);
}

export default rootSaga;